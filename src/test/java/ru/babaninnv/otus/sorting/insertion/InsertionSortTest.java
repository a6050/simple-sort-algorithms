package ru.babaninnv.otus.sorting.insertion;

import org.junit.jupiter.api.DisplayName;
import ru.babaninnv.otus.sorting.test.SortingTestFactory;

@DisplayName("Insertion Sort")
class InsertionSortTest extends SortingTestFactory {
    public void runSort(int[] array) {
        new InsertionSort().sort(array);
    }
}