package ru.babaninnv.otus.sorting.shell;

import org.junit.jupiter.api.DisplayName;
import ru.babaninnv.otus.sorting.test.SortingTestFactory;

@DisplayName("Shell Sort")
class ShellSortTest  extends SortingTestFactory {
    public void runSort(int[] array) {
        new ShellSort().sort(array);
    }
}