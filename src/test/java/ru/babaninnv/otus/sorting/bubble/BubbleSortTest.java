package ru.babaninnv.otus.sorting.bubble;

import org.junit.jupiter.api.DisplayName;
import ru.babaninnv.otus.sorting.test.SortingTestFactory;

@DisplayName("Bubble Sort")
public class BubbleSortTest extends SortingTestFactory {
    public void runSort(int[] array) {
        new BubbleSort().sort(array);
    }
}