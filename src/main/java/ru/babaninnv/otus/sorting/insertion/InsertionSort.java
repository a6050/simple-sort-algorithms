package ru.babaninnv.otus.sorting.insertion;

public class InsertionSort {
    public void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int lastSortedIndex = i - 1;
            while (lastSortedIndex >= 0 && key < arr[lastSortedIndex]) {
                arr[lastSortedIndex + 1] = arr[lastSortedIndex];
                lastSortedIndex--;
            }
            arr[lastSortedIndex + 1] = key;
        }
    }
}
