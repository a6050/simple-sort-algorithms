package ru.babaninnv.otus.sorting.shell;

public class ShellSort {

    public void sort(int[] arr) {
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            for (int i = 0; i + gap < arr.length; i++) {
                int p = i + gap;
                int buffer = arr[p];

                while (p - gap >= 0 && arr[p - gap] > buffer) {
                    arr[p] = arr[p - gap];
                    p -= gap;
                }

                arr[p] = buffer;
            }
        }
    }
}
